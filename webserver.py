from flask import Flask, request, send_from_directory, send_file  
import motor_control as m

def _getVars( requestForm ):
    for data in requestForm:
        print( str(data), ' - ', requestForm[data] )
    m.setSpeed( requestForm['rotation'], requestForm['speed'] )
    m.setCenter( requestForm['power'], requestForm['pickUp'] )
    return 'hi'


app = Flask(__name__)

@app.route('/app/')
def index():
    return send_file( '../ketos-web/index.html' )
@app.route('/app/<path:path>')
def indexFiles(path):
    return send_from_directory( '../ketos-web/', path )


@app.route('/post', methods=['POST'])
@app.route('/app/post', methods=['POST'])
def appPost():
    if request.method == 'POST':
        return _getVars(request.form)