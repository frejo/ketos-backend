from gevent.wsgi import WSGIServer
from webserver import app
import configparser

# Load the configuration
Config = configparser.ConfigParser()
Config.read( "config.ini" )

IP = Config.get('Server', 'Ip')
Port = int( Config.get('Server', 'Port') )

# Run the webserver, listening on the configured ip and port
http_server = WSGIServer((IP, Port), app)
http_server.serve_forever()