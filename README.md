# Ketos backend

backend for Ketos control panel

## Requirements

Python 3, pigpio, flask, gevent, ketos-web

### Installation

Download Python 3 from [Pythons Website](https://www.python.org/downloads/)
or if you use Raspbian or another Debian based OS, use `apt-get install python3`.

``` bash
git clone https://gitlab.com/frejo/ketos-web.git
git clone https://gitlab.com/frejo/ketos-backend.git
pip install flask gevent pigpio
```

**Important!** Make sure to use Python 3. If Python 2 is standard on your system replace `python` and `pip` with `python3` and `pip3`.

## Config

Change the settings in `config.ini` as necessary

## Run

``` bash
python start.py
```

**Important!** Make sure to use Python 3. If Python 2 is standard on your system replace `python` and `pip` with `python3` and `pip3`.

Then connect to the server on `ip:port/app`

## TODO

- Implement minimum motor strenght.