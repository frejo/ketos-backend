import pigpio
import math
import configparser

# Read the config file and import variables: 
Config = configparser.ConfigParser()
Config.read( "config.ini" )

# Set the ip of the Raspberry Pi and the port the pigpio daemon is listening to
ip = Config.get('MotorControl', 'Ip')
port = Config.get('MotorControl', 'Port')

# Getting the GPIO pins the motors are connected to
left_a   = int( Config.get('MotorControl', 'LeftA') )
left_b   = int( Config.get('MotorControl', 'LeftB') )
right_a  = int( Config.get('MotorControl', 'RightA'))
right_b  = int( Config.get('MotorControl', 'RightB'))
cent_a   = int( Config.get('MotorControl', 'CentA') )
cent_b   = int( Config.get('MotorControl', 'CentB') )
left     = [ left_a, left_b ]
right    = [ right_a, right_b ]
cent     = [ cent_a, cent_b ]

# Setting calibration
leftCal  = float( Config.get('MotorControl', 'leftCal') )
rightCal = float( Config.get('MotorControl', 'rightCal'))
centCal  = float( Config.get('MotorControl', 'centCal') )

# Connect to the PiGPIO daemon
pi = pigpio.pi(ip, port)

# Calculate the speed.
def _calcSpeed( speed ):
    # The input is a number between -100 and 100. The PiGPIO uses numbers between 0 and 255, 
    # and we want to be able to seperate forwards and backwards.
    # So if the number is >= 0 we set the diretion to 1, meaning forward, and when it is less 
    # than 0, the direction is -1, meaning backwards.
    ispeed = int(speed)
    if ispeed >= 0:
        direction = 1
    elif ispeed < 0:
        direction = -1
    else:
        print("Speed '{}' is not a number.".format(speed))
        return 1, 0
    
    # Since we want positive numbers for both directions, we take the square root of the 
    # square of the input, making it positive. 
    # Then it is divided by 100 and multiplied by 255 to convert from percent to the 8-bit 
    # number PiGPIO uses. 
    # In case the calculated dutycycle isn't a whole number, the floor (rounded down) is used. 
    # The type is also changed to an int, from a float, because PiGPIO cannot use the number 
    # otherwise.
    ispeed = math.sqrt( math.pow(ispeed, 2) )
    dutycycle = int( math.floor(ispeed / 100 * 255) )

    # Then the direction (1/-1) and the dutycycle (0-255) is returned. 
    return direction, dutycycle

def _calcTurn( rotation ):
    # To turn the boat, we will multiply the dutycycle of each motor according to the 
    # rotation input (between -180 and 180)
    #
    # Example cases: 1) When rotation = 0, we want both motors to turn full speed.
    #                2) When rotation = 180, we want to turn right, so the right motor 
    #                   should not turn, while the left does at 100%
    #                3) When rotation = -90, we want to turn a bit left, so full speed on 
    #                   the right motor and 50% on the left
    #   
    # To do this, we divide the rotation by 180 to get a number (i) between -1 and 1
    irotation = int( rotation )
    i = irotation  / 180
    
    # If the rotation is actually between -180 and 180, 
    # we set the left multiplier to 1 + i and the right multiplier to 1 - i
    # 
    # In case 1), i = 0,
    #             iLeft = 1 + 0 = 1
    #             iRight = 1 - 0 = 1
    #
    #    case 2), i = 1
    #             iLeft = 1 + 1 = 2
    #             iRight = 1 - 1 = 0
    #
    #    case 3), i = -0.5
    #             iLeft = 1 + (-0.5) = 0.5
    #             iRight = 1 - (-0.5) = 1.5
    #
    # The thing is though, that we cannot multiply the dutycycle by more than 1, 
    # since that could give us a dutycycle of up to 510 (255*2), but PiGPIO only 
    # accepts numbers between 0 and 255.
    # So we have to set the multipliers to 1 if it exceeds 1. 
    if irotation >= -180 and irotation <= 180:
        iLeft = 1 + i
        iRight = 1 - i

        # make sure the variables never exceeds [0, 1]
        if iLeft > 1: 
            iLeft = 1
        elif iLeft < 0:
            iLeft = 0
        if iRight > 1: 
            iRight = 1
        elif iRight < 0:
            iRight = 0
    else:
        print("Rotation '{}' is not a valid number.".format(rotation))
    # Then return a list with the left and right multipliers
    return [ iLeft, iRight ]

def setSpeed( rotation, speed ):
    # To set the speed of each motor, we use the program PiGPIO to pulse width modulate 
    # (PWM) the motor outputs. This makes the digital General Purpose Input/Output (GPIO) 
    # pins on the Raspberry Pi act like analog pins. 
    # It accomplishes this by turning the specified pins on and off rapidly and adjusting 
    # how long the pins are on compared to how long they are off. The set_PWM_dutycycle 
    # function sets the dutycycle in a range between 0 and 255, where 0 is off all the 
    # time and 255 is on all the time. 
    
    # Call the the functions that calculate speed and rotation
    turn = _calcTurn( rotation )
    direction, dutycycle = _calcSpeed( speed )
    if direction == 1:
        # If the direction is 1, it means we are going to go forward. So we set the 
        # dutycycle of the a pins (forward) to the dutycycle multiplied with the rotation 
        # and calibration multipliers. And the b pins are set to 0.
        pi.set_PWM_dutycycle( left_a , turn[0] * dutycycle * leftCal )
        pi.set_PWM_dutycycle( right_a , turn[1] * dutycycle * rightCal )
        pi.set_PWM_dutycycle( left_b , 0 )
        pi.set_PWM_dutycycle( right_b , 0 )
    elif direction == -1:
        # The opposite happens when direction is -1, and we want to go back. 
        # Then the a pins are set to 0 and we set the dutycycle of the b pins
        pi.set_PWM_dutycycle( left_a , 0 )
        pi.set_PWM_dutycycle( right_a , 0 )
        pi.set_PWM_dutycycle( left_b , turn[0] * dutycycle * leftCal )
        pi.set_PWM_dutycycle( right_b , turn[1] * dutycycle * rightCal )
    return

def setCenter( power, direction ):
    # The center motor only has 3 states: pull, push and off. If the power is true, 
    # it should be on, and we check whether it should pull or push. 
    # If the direction switch is true, it means we will pull, and the a pin is turned on, 
    # while the b pin is off. 
    # If the direction is false, turn the a pin off and b pin on
    # At last, if the power was not on, it must mean the motor should turn off, 
    # so both pins are set to 0
    if power == 'true':
        if direction == 'true':
            pi.set_PWM_dutycycle( cent_a , 255 * centCal )
            pi.set_PWM_dutycycle( cent_b, 0 )
        else:
            pi.set_PWM_dutycycle( cent_a , 0 )
            pi.set_PWM_dutycycle( cent_b, 255 * centCal )
    else:
        pi.set_PWM_dutycycle( cent_a , 0 )
        pi.set_PWM_dutycycle( cent_b, 0 ) 
